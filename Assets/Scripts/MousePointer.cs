﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MousePointer : MonoBehaviour {

	private static MousePointer mInstance;
	public static MousePointer instance { get {return mInstance; }}
	public SpriteRenderer targetSprite;

	// Use this for initialization
	void Start () {
		if (mInstance == null) {
			mInstance = this;
		}
	}
	
	public void TargetChosen(bool chosen){
		if (chosen) {
			targetSprite.color = Color.red;
		} else {
			targetSprite.color = Color.white;
		}
	}
}
