﻿using UnityEngine;
using System.Collections;

public class FireSound : MonoBehaviour {

	private static FireSound mInstance;
	public static FireSound instance { get {return mInstance; }}
	public AudioSource fireAudio;
	public ParticleSystem recoilparticles;
	public ParticleSystem shot;

	private bool laughing = false;
	public AudioSource laughingAudio;
	
	// Use this for initialization
	void Start () {
		if (mInstance == null) {
			mInstance = this;
		}
	}

	public void fireCube(){
		fire ();

		if (laughing) {
			laughingAudio.loop = false;
			laughingAudio.Stop ();
			laughing = false;
		} else {
			laughingAudio.loop = true;
			laughingAudio.Play();
			laughing = true;
		}
	}

	public void fire(){
		fireAudio.Play();
		recoilparticles.Emit (100);
		shot.Emit (1);
	}


}
